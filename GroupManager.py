from model.VehicleType import VehicleType
from model.FacilityType import FacilityType
import numpy as np


class GroupManager:
    def __init__(self, my_vehicles):
        self.groups = {}
        self.groupless_vehicles = {}

        for v_type in [VehicleType.FIGHTER, VehicleType.HELICOPTER, VehicleType.IFV, VehicleType.TANK, VehicleType.ARRV]:
            self.groups[v_type + 1] = Group(id=v_type + 1, vehicle_type=v_type)

        for v in my_vehicles:
            self.groups[v.type + 1].update_vehicle(v)

        for group in self.groups.values():
            group.update_vehicle_types()

    def update_group_states(self, vehicles, my_id):
        grouped_vehicle_ids = []
        self.groupless_vehicles = {}

        for v_id, vehicle in vehicles.items():
            for group_id in vehicle.groups:
                self.groups[group_id].update_vehicle(vehicles[v_id])

        for group_id in list(self.groups.keys()):
            group = self.groups[group_id]
            group.clear_killed_vehicles(vehicles)
            grouped_vehicle_ids += group.vehicle_ids
            for v_id in group.vehicle_ids:
                group.update_vehicle(vehicles[v_id])

            group.update_vehicle_types()

        for group_id, group in self.groups.items():
            if group.is_disabled:
                continue

            for group_id_2, group_2 in self.groups.items():
                if group_id <= group_id_2 or group_2.is_disabled:
                    continue

                if len(group.vehicles) + len(group_2.vehicles) > 125:
                    continue

                if group.is_aerial != group_2.is_aerial:
                    continue

                group_center = group.center
                group_2_center = group_2.center

                distance = group_center - group_2_center

                if distance.dot(distance) < 1024:
                    group.merging_with = group_id_2
                    group.vehicles.update(group_2.vehicles)
                    group_2.dropped = True

        for v in vehicles.values():
            if v.player_id != my_id:
                continue

            if v.id not in grouped_vehicle_ids:
                self.groupless_vehicles[v.id] = v

    def create_new_groups(self, facilities, game):
        new_groups = {}

        facility_height = game.facility_height
        facility_width = game.facility_width

        for f in facilities:
            new_groups[f.id] = []

            if f.type == FacilityType.CONTROL_CENTER:
                continue

            right = f.left + facility_width
            bottom = f.top + facility_height

            for v in self.groupless_vehicles.values():
                if f.left < v.x < right and f.top < v.y < bottom:
                    new_groups[f.id].append(v.id)

        for new_group in new_groups.values():
            if len(new_group) < self._new_group_size:
                continue

            new_group_id = self._next_id
            self.groups[new_group_id] = Group(id=new_group_id)

            for v_id in new_group:
                self.groups[new_group_id].update_vehicle(self.groupless_vehicles[v_id])
                del self.groupless_vehicles[v_id]

            self.groups[new_group_id].update_vehicle_types()

    @property
    def _next_id(self):
        return max(self.groups.keys()) + 1

    @property
    def _new_group_size(self):
        if len(self.groups) < 10:
            return 25

        if len(self.groups) < 20:
            return 35

        return 45


class Group:
    def __init__(self, id=id, vehicle_type=None):
        self.vehicles = {}
        self.id = id
        self.assigned = False
        self.merging_with = False
        self.vehicle_type = vehicle_type
        self.vehicle_types = set([vehicle.type for vehicle in self.vehicles.values()])
        self.rotating = False
        self.dropped = False

    @property
    def is_disabled(self):
        return self.dropped or len(self.vehicles) == 0

    @property
    def is_aerial(self):
        return self.vehicle_types.issubset([VehicleType.HELICOPTER, VehicleType.FIGHTER])

    def clear_killed_vehicles(self, alive_vehicles):
        for v_id in list(self.vehicles.keys()):
            if v_id not in alive_vehicles:
                del self.vehicles[v_id]

    @property
    def vehicle_ids(self):
        return self.vehicles.keys()

    def update_vehicle(self, vehicle):
        self.vehicles[vehicle.id] = vehicle

    def update_vehicle_types(self):
        self.vehicle_types = set([vehicle.type for vehicle in self.vehicles.values()])

    @property
    def vehicle_points(self):
        return [[v.x, v.y] for v in self.vehicles.values()]

    @property
    def center(self):
        return np.median(self.vehicle_points, axis=0)

    def scaling_needed(self):
        points = self.vehicle_points

        x1 = max(points, key=lambda c: c[0])[0]
        y1 = max(points, key=lambda c: c[1])[1]
        x2 = min(points, key=lambda c: c[0])[0]
        y2 = min(points, key=lambda c: c[1])[1]

        return (x1 - x2) ** 2 + (y1 - y2) ** 2 > len(self.vehicles) * 100

    def max_speed(self):
        reduction_coef = 0.85
        if len(self.vehicles) < 20:
            reduction_coef = 1.0
        return min((map(lambda v: v.max_speed, self.vehicles.values()))) * reduction_coef

    @property
    def left_top(self):
        points = self.vehicle_points

        left = min(points, key=lambda c: c[0])[0]
        top = min(points, key=lambda c: c[1])[1]

        return [left, top]

    @property
    def right_bottom(self):
        points = self.vehicle_points

        right = max(points, key=lambda c: c[0])[0]
        bottom = max(points, key=lambda c: c[1])[1]

        return [right, bottom]
