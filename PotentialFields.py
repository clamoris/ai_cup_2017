import numpy as np
from model.VehicleType import VehicleType
import math
from functools import lru_cache
from scipy.spatial.distance import euclidean


class PotentialFields:
    FIELD_SIZE = 1024
    MAP_SIZE = 64
    UNIT_MAP_SIZE = int(64 / 4)

    def __init__(self, game):
        np.seterr(divide='ignore', invalid='ignore')

        self.facility_height = game.facility_height
        self.facility_width = game.facility_width

        self.fog_of_war = game.fog_of_war_enabled

        self.my_influence_map = {}
        self.enemy_influence_map = {}
        self.enemy_gravity_maps = {}
        self.my_map = np.zeros((self.MAP_SIZE, self.MAP_SIZE))
        self.enemy_map = np.zeros((self.MAP_SIZE, self.MAP_SIZE))

        self.enemy_health_map = {}
        self.my_health_map = {}

        self.enemy_units_presence = {}

        self.enemy_units_map = {}
        self.my_units_map = {}

        self.my_units_move_map = {}

        self.enemy_units_by_x_y = np.empty((self.MAP_SIZE, self.MAP_SIZE))
        self.my_units_by_x_y = np.empty((self.MAP_SIZE, self.MAP_SIZE))

        self.my_unit_counts = {}
        self.enemy_unit_counts = {}

        self.my_total_unit_count = 0
        self.enemy_total_unit_count = 0

        self.facilities_gravity_map = np.zeros((self.MAP_SIZE, self.MAP_SIZE))
        self.healers_map = np.zeros((self.MAP_SIZE, self.MAP_SIZE))
        self.ifv_map = np.zeros((self.MAP_SIZE, self.MAP_SIZE))

        self.per_facility_maps = []

        self.enemy_damage_map = {}
        self.my_damage_map = {}

        self._facilities = []
        self._my_id = -1
        self._enemy_id = -1

    def build_pp(self, units_map, field, point_field, normalize=False, all_count=1.0):
        if normalize and all_count != 0:
            point_field = point_field / all_count

        for index, value in np.ndenumerate(units_map):
            if value == 0:
                continue
            self.pp_step(index[0], index[1], field, point_field * value)

    @staticmethod
    def pp_step(x, y, field, point_field):
        map_size = field.shape[0]
        max_shape = point_field.shape[0]
        border = max_shape // 2

        x_min = max(0, x - border)
        x_max = min(map_size, x + border + 1)

        y_min = max(0, y - border)
        y_max = min(map_size, y + border + 1)

        my_x_min = max(0, -(x - border))
        my_y_min = max(0, -(y - border))

        my_x_max = max_shape + min(map_size - (x + border + 1), 0)
        my_y_max = max_shape + min(map_size - (y + border + 1), 0)

        field[slice(x_min, x_max), slice(y_min, y_max)] += \
            point_field[slice(my_x_min, my_x_max), slice(my_y_min, my_y_max)]

    def build_maps(self, vehicles, world):
        self.build_unit_maps(vehicles)

        self._facilities = world.facilities
        self._my_id = world.get_my_player().id
        self._enemy_id = world.get_opponent_player().id

        self.build_my_map()
        self.build_enemy_gravity()
        self.build_facility_maps()
        self.build_healers_maps()
        self.build_ifv_maps()
        self.build_damage_maps()

    def build_facility_maps(self):
        if len(self._facilities) == 0:
            return

        self.build_per_facility_map()

        self.facilities_gravity_map = np.max(self.per_facility_maps, axis=0)

    def build_per_facility_map(self):
        self.per_facility_maps = []

        for f in self._facilities:
            x = int((f.left + (0.5 * self.facility_width)) / (self.FIELD_SIZE / self.MAP_SIZE))
            y = int((f.top + (0.5 * self.facility_height)) / (self.FIELD_SIZE / self.MAP_SIZE))

            field = np.zeros((self.MAP_SIZE, self.MAP_SIZE))

            if f.owner_player_id != self._my_id:
                self.pp_step(x, y, field, self.gravity_point())
                self.pp_step(x, y, field, self.facility_map_point())

            if f.owner_player_id == self._my_id:
                self.pp_step(x, y, field, -self.facility_repel_point())

            self.per_facility_maps.append(field)

    def build_unit_maps(self, vehicles):
        filler = np.frompyfunc(lambda x: list(), 1, 1)
        factor = self.FIELD_SIZE / self.MAP_SIZE

        self.enemy_units_by_x_y = np.empty((self.MAP_SIZE, self.MAP_SIZE), dtype=np.object)
        filler(self.enemy_units_by_x_y, self.enemy_units_by_x_y)
        self.my_units_by_x_y = np.empty((self.MAP_SIZE, self.MAP_SIZE), dtype=np.object)
        filler(self.my_units_by_x_y, self.my_units_by_x_y)

        for i in VehicleType:
            self.enemy_units_presence[i] = np.zeros((self.MAP_SIZE, self.MAP_SIZE))

            self.enemy_units_map[i] = np.zeros((self.MAP_SIZE, self.MAP_SIZE))
            self.my_units_map[i] = np.zeros((self.MAP_SIZE, self.MAP_SIZE))

            self.my_units_move_map[i] = np.zeros((self.MAP_SIZE, self.MAP_SIZE))

            self.enemy_health_map[i] = np.zeros((self.MAP_SIZE, self.MAP_SIZE))
            self.my_health_map[i] = np.zeros((self.MAP_SIZE, self.MAP_SIZE))

            self.my_unit_counts[i] = 0
            self.enemy_unit_counts[i] = 0

        for v in vehicles:
            unit_x = int(v.x / factor)
            unit_y = int(v.y / factor)

            if v.player_id == self._my_id:
                self.my_units_map[v.type][unit_x, unit_y] += 1
                self.my_health_map[v.type][unit_x, unit_y] += round(v.durability / 10.0, 1)
                self.my_units_by_x_y[unit_x, unit_y].append(v)
                self.my_unit_counts[v.type] += 1

                if hasattr(v, 'move_x'):
                    move_x = max(0, min(int(v.move_x / factor), self.MAP_SIZE - 1))
                    move_y = max(0, min(int(v.move_y / factor), self.MAP_SIZE - 1))

                    self.my_units_move_map[v.type][move_x, move_y] += 1
                else:
                    self.my_units_move_map[v.type][unit_x, unit_y] += 1

            else:
                self.enemy_units_presence[v.type][unit_x, unit_y] = 1
                self.enemy_units_map[v.type][unit_x, unit_y] += 1
                self.enemy_health_map[v.type][unit_x, unit_y] += round(v.durability / 10.0, 1)
                self.enemy_units_by_x_y[unit_x, unit_y].append(v)
                self.enemy_unit_counts[v.type] += 1

        self.my_total_unit_count = sum(self.my_unit_counts.values())
        self.enemy_total_unit_count = sum(self.enemy_unit_counts.values())
        self.enemy_map = sum(self.enemy_units_map.values())

    def build_damage_maps(self):
        for my_type in VehicleType:
            self.enemy_damage_map[my_type] = np.zeros((self.MAP_SIZE, self.MAP_SIZE))
            damage_to_me = np.zeros((self.MAP_SIZE, self.MAP_SIZE))
            for enemy_type in VehicleType:
                if self.enemy_unit_counts[enemy_type] == 0:
                    continue

                damage_to_me += self.enemy_units_map[enemy_type] * self.damage(enemy_type)[my_type] * \
                                self.enemy_health_map[enemy_type] #* 1.1

            for index, value in np.ndenumerate(damage_to_me):
                if value == 0:
                    continue

                self.pp_step(index[0], index[1], self.enemy_damage_map[my_type], self.damage_map_point() * -value)

    def build_my_map(self):
        for i in VehicleType:
            self.my_influence_map[i] = np.zeros((self.MAP_SIZE, self.MAP_SIZE))
            if self.my_unit_counts[i] > 0:
                self.build_pp(self.my_units_map[i], self.my_influence_map[i], self.my_map_point(),
                              normalize=True, all_count=self.my_unit_counts[i])

        self.my_map = sum(self.my_influence_map.values())

    def build_enemy_gravity(self):
        for i in VehicleType:
            self.enemy_gravity_maps[i] = np.zeros((self.MAP_SIZE, self.MAP_SIZE))
            if self.enemy_unit_counts[i] > 0:
                self.build_pp(self.enemy_units_map[i], self.enemy_gravity_maps[i], self.gravity_point(),
                              normalize=True, all_count=float(self.enemy_unit_counts[i]))

    def build_healers_maps(self):
        self.healers_map = np.zeros((self.MAP_SIZE, self.MAP_SIZE))

        if self.my_unit_counts[VehicleType.ARRV] > 0:
            self.build_pp(self.my_units_map[VehicleType.ARRV], self.healers_map, self.gravity_point(1.1))

    def build_ifv_maps(self):
        self.ifv_map = np.zeros((self.MAP_SIZE, self.MAP_SIZE))

        if self.my_unit_counts[VehicleType.IFV] > 0:
            self.build_pp(self.my_units_move_map[VehicleType.IFV], self.ifv_map, self.gravity_point(1.1))

    @staticmethod
    @lru_cache(maxsize=None, typed=False)
    def damage_map_point():
        pp_point = np.ones((3, 3))
        return np.pad(pp_point, (1, 1), 'constant', constant_values=0.5)

    @staticmethod
    @lru_cache(maxsize=None, typed=False)
    def facility_map_point():
        gravitation_coef = 5
        pp_point = np.ones((5, 5))
        for i in range(6):
            pp_point[i:5 - i, i:5 - i] = pp_point[i:5 - i, i:5 - i] * gravitation_coef
        return pp_point

    @staticmethod
    @lru_cache(maxsize=None, typed=False)
    def facility_repel_point():
        gravitation_coef = 2
        pp_point = np.ones((9, 9))
        for i in range(10):
            pp_point[i:9 - i, i:9 - i] = pp_point[i:9 - i, i:9 - i] * gravitation_coef
        return pp_point

    @staticmethod
    @lru_cache(maxsize=None, typed=False)
    def enemy_map_point():
        gravitation_coef = 2
        pp_point = np.ones((9, 9))
        for i in range(10):
            pp_point[i:9 - i, i:9 - i] = pp_point[i:9 - i, i:9 - i] * gravitation_coef
        return pp_point

    @staticmethod
    @lru_cache(maxsize=None, typed=False)
    def my_map_point():
        pp_point = np.ones((7, 7))
        pp_point[1:6, 1:6] = pp_point[1:6, 1:6] * 2
        pp_point[2:5, 2:5] = pp_point[2:5, 2:5] * 2
        pp_point[3, 3] = 16

        pp_point[0, 0] = pp_point[0, 6] = pp_point[6, 0] = pp_point[6, 6] = 0
        pp_point[1, 1] = pp_point[1, 5] = pp_point[5, 1] = pp_point[5, 5] = 1

        return pp_point * 8

    @lru_cache(maxsize=None, typed=False)
    def gravity_point(self, gravitation_coef=1.01):
        pp_point = np.ones((self.MAP_SIZE * 2 + 1, self.MAP_SIZE * 2 + 1))
        center = np.array([self.MAP_SIZE, self.MAP_SIZE])

        for index, value in np.ndenumerate(pp_point):
            pp_point[index] = gravitation_coef ** (self.MAP_SIZE + 1 - euclidean(index, center))

        return pp_point

    def optimal_coords(self, group):
        x, y = group.center

        distances = [16, 32]
        if VehicleType.HELICOPTER in group.vehicle_types or VehicleType.FIGHTER in group.vehicle_types:
            distances += [48]
        if VehicleType.FIGHTER in group.vehicle_types:
            distances += [64]

        choices = [[x, y]]
        for max_distance in distances:
            for angle in np.linspace(22.5, 360, 16):
                angle = angle * math.pi / 180
                choices.append([max_distance * math.cos(angle) + x, max_distance * math.sin(angle) + y])

        for choice in choices:
            choice[0] = max(0, choice[0])
            choice[0] = min(self.FIELD_SIZE - 1, choice[0])

            choice[1] = max(0, choice[1])
            choice[1] = min(self.FIELD_SIZE - 1, choice[1])

        field = self.field_for_group(group)

        field_scores = []
        max_potential = float("-inf")
        max_potential_index = -1

        for choice in choices:
            score = self.field_score(field, choice, group)
            if score > max_potential:
                max_potential = score
                max_potential_index = choices.index(choice)

            field_scores.append(score)

        return choices[max_potential_index], max_potential - field_scores[0], field

    def field_for_group(self, group):
        group_vehicles = group.vehicles.values()

        field = np.zeros((self.MAP_SIZE - 2, self.MAP_SIZE - 2))
        field = np.pad(field, (1, 1), 'constant', constant_values=-1000)

        group.power_field = {}

        for vehicle_type in group.vehicle_types:
            vehicles = [v for v in group_vehicles if v.type == vehicle_type]

            field += self.enemy_damage_map[vehicle_type]

            temp_fields = []
            for key, weight in self.enemy_gravity_weights(vehicle_type).items():
                if weight == 0:
                    continue
                temp_fields.append(self.enemy_gravity_maps[key] * weight)

            temp_fields.append(self.facilities_gravity_map * self.facility_gravity_weights(vehicle_type))

            group.power_field[vehicle_type] = self.build_power_field(vehicles)

            temp_fields.append(self.healers_map * self.low_health_coef(vehicles))

            if vehicle_type == VehicleType.HELICOPTER:
                temp_fields.append(self.ifv_map * self.fighters_danger_coef())

            if self.fog_of_war:
                if vehicle_type == VehicleType.HELICOPTER and self.enemy_total_unit_count == 0:
                    temp_field = np.zeros((self.MAP_SIZE, self.MAP_SIZE))
                    self.pp_step(self.MAP_SIZE // 2, self.MAP_SIZE // 2, temp_field, self.gravity_point())
                    temp_fields.append(temp_field)
                if vehicle_type == VehicleType.FIGHTER and self.enemy_total_unit_count == 0:
                    temp_field = np.zeros((self.MAP_SIZE, self.MAP_SIZE))
                    self.pp_step(self.MAP_SIZE - 10, self.MAP_SIZE - 10, temp_field, self.gravity_point())
                    temp_fields.append(temp_field)

            if len(temp_fields) > 0:
                field += np.max(temp_fields, axis=0)

        if len(group.vehicle_types) == 1:
            for key, value in self.my_repel_weights(next(iter(group.vehicle_types))).items():
                if value == 0:
                    continue
                field += self.my_influence_map[key] * value
        else:
            field += self.build_group_repel_field(group)

        return field

    def build_group_repel_field(self, group):
        group_unit_maps = {}

        repel_field = np.zeros((self.MAP_SIZE, self.MAP_SIZE))

        factor = (self.FIELD_SIZE / self.MAP_SIZE)

        for v in group.vehicles.values():
            x = int(v.x / factor)
            y = int(v.y / factor)

            if v.type not in group_unit_maps:
                group_unit_maps[v.type] = np.zeros((self.MAP_SIZE, self.MAP_SIZE))

            group_unit_maps[v.type][x, y] += 1

        for vehicle_type in group.vehicle_types:
            field = np.zeros((self.MAP_SIZE, self.MAP_SIZE))

            self.build_pp(group_unit_maps[vehicle_type], field, self.my_map_point(),
                          normalize=True, all_count=self.my_unit_counts[vehicle_type])

            for key, value in self.my_repel_weights(vehicle_type).items():
                if value == 0:
                    continue

                if key in group_unit_maps:
                    field_without_group = self.my_influence_map[key] - field
                else:
                    field_without_group = self.my_influence_map[key]

                repel_field += field_without_group * value

        return repel_field

    def fighters_danger_coef(self):
        if self.enemy_is_scattered or self.enemy_unit_counts[VehicleType.FIGHTER] < 5:
            return 0

        fighters_diff = self.enemy_unit_counts[VehicleType.FIGHTER] - self.my_unit_counts[VehicleType.FIGHTER]

        if fighters_diff > 30:
            return 10
        elif fighters_diff > 10:
            return 5
        elif fighters_diff > -10:
            return 2
        else:
            return 0

    @property
    def enemy_is_scattered(self):
        return np.sum(self.enemy_units_presence[VehicleType.FIGHTER]) > 20

    def build_power_field(self, vehicles):
        pp_point = np.ones((3, 3))
        pp_point = np.pad(pp_point, (1, 1), 'constant', constant_values=0.5)

        power_field = {}

        for i in range(5):
            power_field[i] = np.zeros((self.MAP_SIZE, self.MAP_SIZE))

        factor = (self.FIELD_SIZE / self.MAP_SIZE)

        for v in vehicles:
            if v.type == VehicleType.ARRV:
                continue

            x = int(v.x / factor)
            y = int(v.y / factor)

            for enemy_type in self.damage(v.type):
                damage = self.damage(v.type)[enemy_type]
                if damage == 0:
                    continue
                self.pp_step(x, y, power_field[enemy_type], pp_point * self.damage(v.type)[enemy_type])

        return power_field

    def danger_zone(self, real_coords, group):
        my_center = group.center

        factor = (self.FIELD_SIZE / self.MAP_SIZE)

        my_x = int(my_center[0] / factor)
        my_y = int(my_center[1] / factor)

        new_x = int(real_coords[0] / factor)
        new_y = int(real_coords[1] / factor)

        shift = ((new_x - my_x), (new_y - my_y))

        result = {}

        for vehicle_type in group.vehicle_types:
            fields = {}

            for i in VehicleType:
                if vehicle_type not in group.power_field:
                    fields[i] = np.zeros((self.MAP_SIZE, self.MAP_SIZE))
                    continue

                field = self.roll(group.power_field[vehicle_type][i] * self.my_health_map[vehicle_type], shift, axis=(0, 1))
                fields[i] = field * self.enemy_units_presence[i]

                if i == VehicleType.ARRV:
                    fields[i] += self.enemy_units_map[i] * self.enemy_health_map[i] * -20

            result[vehicle_type] = sum(fields.values())

        return sum(result.values())

    def field_score(self, field, real_coords, group):
        return self.field_value(field + self.danger_zone(real_coords, group), real_coords)

    @staticmethod
    def low_health_coef(vehicles):
        if len(vehicles) == 0:
            return 0

        total_health = 0.0
        for v in vehicles:
            if v.type == VehicleType.HELICOPTER or v.type == VehicleType.FIGHTER:
                total_health += v.durability

        if total_health == 0.0:
            return 0

        average_health = total_health / len(vehicles)

        if average_health >= 85:
            return 0

        return (100 - average_health) / 20

    @staticmethod
    def field_value(field, coords, border=1, output="mean", real=True):
        map_size = field.shape[0]

        if real:
            x = int(coords[0] / (PotentialFields.FIELD_SIZE / map_size))
            y = int(coords[1] / (PotentialFields.FIELD_SIZE / map_size))
        else:
            x = coords[0]
            y = coords[1]

        x_min = max(0, x - border)
        x_max = min(map_size, x + border + 1)

        y_min = max(0, y - border)
        y_max = min(map_size, y + border + 1)

        return getattr(field[x_min:x_max, y_min:y_max], output)()

    @staticmethod
    @lru_cache(maxsize=None, typed=False)
    def enemy_gravity_weights(vehicle_type):
        if vehicle_type == VehicleType.ARRV:
            return {
                VehicleType.FIGHTER: 0,
                VehicleType.HELICOPTER: 0,
                VehicleType.IFV: 0,
                VehicleType.TANK: 0,
                VehicleType.ARRV: 0,
            }
        if vehicle_type == VehicleType.FIGHTER:
            return {
                VehicleType.FIGHTER: 1,
                VehicleType.HELICOPTER: 2,
                VehicleType.IFV: 0,
                VehicleType.TANK: 0,
                VehicleType.ARRV: 0,
            }
        if vehicle_type == VehicleType.HELICOPTER:
            return {
                VehicleType.FIGHTER: 0,
                VehicleType.HELICOPTER: 1,
                VehicleType.IFV: 1,
                VehicleType.TANK: 2,
                VehicleType.ARRV: 3,
            }
        if vehicle_type == VehicleType.IFV:
            return {
                VehicleType.FIGHTER: 3,
                VehicleType.HELICOPTER: 2,
                VehicleType.IFV: 1,
                VehicleType.TANK: 1,
                VehicleType.ARRV: 2,
            }
        if vehicle_type == VehicleType.TANK:
            return {
                VehicleType.FIGHTER: 0,
                VehicleType.HELICOPTER: 0,
                VehicleType.IFV: 2,
                VehicleType.TANK: 1,
                VehicleType.ARRV: 2,
            }

    @staticmethod
    @lru_cache(maxsize=None, typed=False)
    def my_repel_weights(vehicle_type):
        if vehicle_type == VehicleType.ARRV:
            return {
                VehicleType.FIGHTER: 0,
                VehicleType.HELICOPTER: 0,
                VehicleType.IFV: -1,
                VehicleType.TANK: -1,
                VehicleType.ARRV: 0,
            }
        if vehicle_type == VehicleType.FIGHTER:
            return {
                VehicleType.FIGHTER: 0,
                VehicleType.HELICOPTER: -1,
                VehicleType.IFV: 0,
                VehicleType.TANK: 0,
                VehicleType.ARRV: 0,
            }
        if vehicle_type == VehicleType.HELICOPTER:
            return {
                VehicleType.FIGHTER: -1,
                VehicleType.HELICOPTER: 0,
                VehicleType.IFV: 0,
                VehicleType.TANK: 0,
                VehicleType.ARRV: 0,
            }
        if vehicle_type == VehicleType.IFV:
            return {
                VehicleType.FIGHTER: 0,
                VehicleType.HELICOPTER: 0,
                VehicleType.IFV: 0,
                VehicleType.TANK: -1,
                VehicleType.ARRV: -1,
            }
        if vehicle_type == VehicleType.TANK:
            return {
                VehicleType.FIGHTER: 0,
                VehicleType.HELICOPTER: 0,
                VehicleType.IFV: -1,
                VehicleType.TANK: 0,
                VehicleType.ARRV: -1,
            }

    @staticmethod
    @lru_cache(maxsize=None, typed=False)
    def facility_gravity_weights(vehicle_type):
        if vehicle_type == VehicleType.ARRV:
            return 2
        if vehicle_type == VehicleType.FIGHTER:
            return 0
        if vehicle_type == VehicleType.HELICOPTER:
            return 0
        if vehicle_type == VehicleType.IFV:
            return 2
        if vehicle_type == VehicleType.TANK:
            return 2

    @staticmethod
    @lru_cache(maxsize=None, typed=False)
    def damage(vehicle_type):
        if vehicle_type == VehicleType.ARRV:
            return {
                VehicleType.FIGHTER: 0,
                VehicleType.HELICOPTER: 0,
                VehicleType.IFV: 0,
                VehicleType.TANK: 0,
                VehicleType.ARRV: 0,
            }
        if vehicle_type == VehicleType.FIGHTER:
            return {
                VehicleType.FIGHTER: 30,
                VehicleType.HELICOPTER: 60,
                VehicleType.IFV: 0,
                VehicleType.TANK: 0,
                VehicleType.ARRV: 0,
            }
        if vehicle_type == VehicleType.HELICOPTER:
            return {
                VehicleType.FIGHTER: 10,
                VehicleType.HELICOPTER: 40,
                VehicleType.IFV: 20,
                VehicleType.TANK: 40,
                VehicleType.ARRV: 80,
            }
        if vehicle_type == VehicleType.IFV:
            return {
                VehicleType.FIGHTER: 10,
                VehicleType.HELICOPTER: 40,
                VehicleType.IFV: 30,
                VehicleType.TANK: 10,
                VehicleType.ARRV: 40,
            }
        if vehicle_type == VehicleType.TANK:
            return {
                VehicleType.FIGHTER: 0,
                VehicleType.HELICOPTER: 20,
                VehicleType.IFV: 40,
                VehicleType.TANK: 20,
                VehicleType.ARRV: 50,
            }

    def roll(self, matrix, shift, axis):
        return self.roll_zeropad(self.roll_zeropad(matrix, shift[0], axis=axis[0]), shift[1], axis=axis[1])

    @staticmethod
    def roll_zeropad(a, shift, axis=None):
        a = np.asanyarray(a)
        if shift == 0:
            return a
        if axis is None:
            n = a.size
            reshape = True
        else:
            n = a.shape[axis]
            reshape = False
        if np.abs(shift) > n:
            res = np.zeros_like(a)
        elif shift < 0:
            shift += n
            zeros = np.zeros_like(a.take(np.arange(n - shift), axis))
            res = np.concatenate((a.take(np.arange(n - shift, n), axis), zeros), axis)
        else:
            zeros = np.zeros_like(a.take(np.arange(n - shift, n), axis))
            res = np.concatenate((zeros, a.take(np.arange(n - shift), axis)), axis)
        if reshape:
            return res.reshape(a.shape)
        else:
            return res
