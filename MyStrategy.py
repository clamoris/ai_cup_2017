from model.VehicleType import VehicleType
from model.Game import Game
from model.Move import Move
from model.Player import Player
from model.World import World
from model.FacilityType import FacilityType

from MoveManager import MoveManager
from PotentialFields import PotentialFields
from GroupManager import GroupManager

import os
from functools import lru_cache
import numpy as np


if os.getenv('DEBUG_STRATEGY', False):
    from RewindClient import *

    import pandas as pd
    import seaborn as sns

    from matplotlib import pyplot


class MyStrategy:
    @staticmethod
    def debug():
        return os.getenv('DEBUG_STRATEGY', False)

    def move(self, me: Player, world: World, game: Game, move: Move):
        if world.tick_index == 0:
            self.initialize_strategy(world, game)
            if self.debug():
                self.client = RewindClient()

        self.initialize_tick(me, world, game, move)

        if me.remaining_action_cooldown_ticks > 0:
            return

        if world.tick_index == 0:
            self.group_manager = GroupManager(self.my_vehicles)

        if self.debug():
            if hasattr(self, "nuke") and (self.nuke[2] + 30) > self.world.tick_index:
                self.client.circle(self.nuke[0], self.nuke[1], 5, 0x00FF0000, layer=5)
                self.client.circle(self.nuke[0], self.nuke[1], 50, 0xAAFF0000, layer=5)

            if hasattr(self, "nuke") and (self.nuke[2] + 90) > self.world.tick_index and\
                            (self.nuke[2] + 30) < self.world.tick_index:
                self.client.circle(self.nuke[0], self.nuke[1], 5, 0x00FF0000, layer=5)
                self.client.circle(self.nuke[0], self.nuke[1], 50, 0x70FF0000, layer=5)

        self.plan_move()
        self.move_manager.make_move(move, self.vehicles.values())

        if self.debug():
            types = {
                VehicleType.ARRV: UnitType.ARRV,
                VehicleType.FIGHTER: UnitType.FIGHTER,
                VehicleType.HELICOPTER: UnitType.HELICOPTER,
                VehicleType.IFV: UnitType.IFV,
                VehicleType.TANK: UnitType.TANK
            }

            for v in self.vehicles.values():
                enemy = Side.ALLY if v.player_id == self.me.id else Side.ENEMY
                self.client.living_unit(v.x, v.y, v.radius, v.durability, v.max_durability,
                                        v.remaining_attack_cooldown_ticks, v.attack_cooldown_ticks,
                                        v.selected, enemy, unit_type=types[v.type])
                if hasattr(v, 'move_x'):
                    self.client.line(v.x, v.y, v.move_x, v.move_y, 0x00FFFFFF)

            self.client.end_frame()

    def initialize_strategy(self, world, game):
        self.vehicles = {}
        self.updated_vehicles = {}
        self.move_manager = MoveManager(world, game)
        self.pf = PotentialFields(game)

        self.last_scale = {}
        self.evading = False

    def visibility_for_vehicle(self, vehicle):
        return vehicle.vision_range * self.visibility_factor(int(vehicle.x / 32), int(vehicle.y / 32), vehicle.aerial)

    @staticmethod
    @lru_cache(maxsize=None, typed=False)
    def vision_range(vehicle_type):
        return {
            VehicleType.TANK: 80,
            VehicleType.IFV: 80,
            VehicleType.HELICOPTER: 100,
            VehicleType.FIGHTER: 120,
            VehicleType.ARRV: 60
        }[vehicle_type]

    @lru_cache(maxsize=None, typed=False)
    def visibility_factor(self, x, y, aerial):
        if aerial:
            return {0: 1.0, 1: 0.8, 2: 0.6}[self.world.weather_by_cell_x_y[x][y]]
        else:
            return {0: 1.0, 1: 1.0, 2: 0.8}[self.world.terrain_by_cell_x_y[x][y]]

    def initialize_tick(self, me, world, game, move):
        self.me = me
        self.world = world
        self.game = game
        self.current_move = move

        self.move_manager.update_state(world, me.remaining_action_cooldown_ticks, self.owned_cc_count)

        for vehicle in world.new_vehicles:
            self.vehicles[vehicle.id] = vehicle
            self.updated_vehicles[vehicle.id] = world.tick_index

        for vehicle_update in world.vehicle_updates:
            if vehicle_update.durability == 0:
                del self.vehicles[vehicle_update.id]
                del self.updated_vehicles[vehicle_update.id]
            else:
                self.vehicles[vehicle_update.id].update(vehicle_update)
                self.updated_vehicles[vehicle_update.id] = world.tick_index

    @property
    def owned_cc_count(self):
        per_cc_boost = self.game.additional_action_count_per_control_center
        owned_cc = [f for f in self.world.facilities if f.owner_player_id == self.me.id and f.type == FacilityType.CONTROL_CENTER]
        return len(owned_cc) * per_cc_boost

    @property
    def my_vehicles(self):
        return [v for v in self.vehicles.values() if v.player_id == self.me.id]

    @property
    def enemy_vehicles(self):
        return [v for v in self.vehicles.values() if v.player_id != self.me.id]

    def evade_enemy_nuke(self):
        opponent = self.world.get_opponent_player()
        if opponent.next_nuclear_strike_vehicle_id != -1:
            boom_in = opponent.next_nuclear_strike_tick_index - self.world.tick_index
            if not self.evading and self.move_manager.remaining_actions >= 2 and boom_in > 5:
                self.evading = True
                self.move_manager.plan_evading(opponent.next_nuclear_strike_x, opponent.next_nuclear_strike_y, boom_in)
        else:
            self.evading = False

    def plan_move(self):
        self.evade_enemy_nuke()

        if self.world.tick_index % 60 == 0:
            if self.world.tick_index != 0:
                self.group_manager.update_group_states(self.vehicles, self.me.id)
                self.group_manager.create_new_groups(self.world.facilities, self.game)

            self.pf.build_maps(self.vehicles.values(), self.world)
            self.plan_nuking()
            self.plan_building()

            choices = []

            enemy_units_all = sum(self.pf.enemy_units_map.values())

            for group in self.group_manager.groups.values():
                if self.world.tick_index == 0 and group.vehicle_type == VehicleType.HELICOPTER:
                    continue

                if group.is_disabled:
                    continue

                my_point = group.center

                x = my_point[0]
                y = my_point[1]

                if self.pf.field_value(enemy_units_all, my_point, border=3, output="sum") < 10:
                    if not self.evading and group.id not in self.last_scale and group.scaling_needed():
                        self.move_manager.plan_selecting(group_id=group.id)
                        self.move_manager.plan_scaling(x, y)
                        self.last_scale[group.id] = self.world.tick_index
                        continue

                    if not self.evading and group.id in self.last_scale and\
                            self.last_scale[group.id] > 200 and group.scaling_needed() and not group.rotating:
                        self.move_manager.plan_selecting(group_id=group.id)
                        self.move_manager.plan_rotating(x, y)
                        group.rotating = True
                        continue

                    if not self.evading and group.id in self.last_scale and\
                            self.last_scale[group.id] > 250 and group.scaling_needed() and group.rotating:
                        self.move_manager.plan_selecting(group_id=group.id)
                        self.move_manager.plan_scaling(x, y)
                        continue

                if group.id in self.last_scale and self.world.tick_index - self.last_scale[group.id] > 350:
                    del self.last_scale[group.id]
                    group.rotating = False

                optimal, potential_change, _field = self.pf.optimal_coords(group)

                # if VehicleType.ARRV in group.vehicle_types:
                #     max_value = np.max(_field)
                #     min_value = np.min(_field)
                #
                #     for index, value in np.ndenumerate(_field):
                #         x = index[0]
                #         y = index[1]
                #         if value > 0:
                #             val = int(255 * value / max_value)
                #             if val > 0:
                #                 self.client.rectangle(x * 16, y * 16, (x + 1) * 16, (y + 1) * 16,
                #                                           0x00FFFF00 | val << 24,
                #                                           layer=1)
                #
                #         if value < 0:
                #             val = int(255 * value / min_value)
                #             if val > 0:
                #                 self.client.rectangle(x * 16, y * 16, (x + 1) * 16, (y + 1) * 16,
                #                                           0x00FF0000 | val << 24,
                #                                           layer=1)

                if (my_point != optimal).any():
                    choices.append([group, optimal, potential_change])

            choices.sort(key=lambda c: c[2], reverse=True)

            max_groups_to_process = (self.move_manager.remaining_actions - len(self.move_manager.moves)) // 2
            for choice in choices[:max_groups_to_process + 1]:
                group = choice[0]
                optimal = choice[1]
                my_point = group.center

                x = optimal[0] - my_point[0]
                y = optimal[1] - my_point[1]

                if group.merging_with:
                    self.move_manager.plan_selecting(group_id=group.id)
                    self.move_manager.plan_selecting(group_id=group.merging_with, add=True)
                    self.move_manager.plan_group_assignment(group.id)
                    group.merging_with = False
                elif not group.assigned:
                    self.move_manager.plan_selecting(group.left_top, group.right_bottom, group.vehicle_type)
                    self.move_manager.plan_group_assignment(group.id)
                    group.assigned = True

                    if group.vehicle_type == VehicleType.FIGHTER or group.id > 5:
                        self.move_manager.plan_moving(x=x, y=y, max_speed=group.max_speed(), select=False)
                    else:
                        self.move_manager.plan_scaling(my_point[0], my_point[1], factor=0.75)
                else:
                    self.move_manager.plan_moving(x=x, y=y, max_speed=group.max_speed(), group_id=group.id)

    def plan_nuking(self):
        if self.me.remaining_nuclear_strike_cooldown_ticks > 0 or self.world.tick_index < 350:
            return
        if self.pf.enemy_total_unit_count == 0:
            return

        nuker_cache = {}
        nuking_my_units_cache = {}
        nuking_enemy_units_cache = {}

        factor = self.pf.FIELD_SIZE / self.pf.MAP_SIZE

        enemy_units_all = sum(self.pf.enemy_units_map.values())
        my_units_all = sum(self.pf.my_units_map.values())

        choices = []

        for my_x_y, my_units in np.ndenumerate(self.pf.my_units_by_x_y):
            if len(my_units) == 0:
                continue

            if self.pf.field_value(enemy_units_all, my_x_y, output="sum", border=7, real=False) == 0:
                continue

            for my_unit in my_units:
                if my_unit.type not in nuker_cache:
                    nuker_cache[my_unit.type] = np.zeros_like(self.pf.my_units_by_x_y, dtype=bool)

                if nuker_cache[my_unit.type][my_x_y]:
                    continue

                visibility_for_vehicle = self.visibility_for_vehicle(my_unit) - 10

                for enemy_x_y, enemy_units in np.ndenumerate(enemy_units_all):
                    if enemy_units == 0 or abs(enemy_x_y[0] - my_x_y[0]) > 7 or abs(enemy_x_y[1] - my_x_y[1]) > 7:
                        continue

                    if enemy_x_y not in nuking_my_units_cache:
                        nuking_my_units_cache[enemy_x_y] = self.pf.field_value(my_units_all, enemy_x_y,
                                                                               output="sum", real=False)

                    if nuking_my_units_cache[enemy_x_y] > 0:
                        continue

                    nuke_point = (np.array(enemy_x_y) + np.array([0.5, 0.5])) * factor

                    if (my_unit.get_squared_distance_to(nuke_point[0], nuke_point[1]) ** 0.5) <= visibility_for_vehicle:
                        if enemy_x_y not in nuking_enemy_units_cache:
                            all_units = self.pf.field_value(enemy_units_all, enemy_x_y, output="sum", real=False)
                            arrv = self.pf.field_value(self.pf.enemy_units_map[VehicleType.ARRV], enemy_x_y,
                                                       output="sum", real=False)
                            nuking_enemy_units_cache[enemy_x_y] = [all_units, arrv]

                        if nuking_enemy_units_cache[enemy_x_y][0] < len(self.enemy_vehicles) / 25:
                            continue

                        choices.append([nuking_enemy_units_cache[enemy_x_y], nuke_point, my_unit.id])

        if len(choices) == 0:
            return

        # Sort by number of affected units minus half of arrv count in them
        choices.sort(key=lambda c: c[0][0] - (c[0][1] / 2), reverse=True)

        nuke_point = choices[0][1]
        nuker = choices[0][2]

        self.move_manager.plan_nuking(nuke_point[0], nuke_point[1], nuker)

        if self.debug():
            self.nuke = [nuke_point[0], nuke_point[1], self.world.tick_index]
            self.client.circle(self.nuke[0], self.nuke[1], 5, 0x00FF0000, layer=5)

    def plan_building(self):
        fighters_diff = self.pf.enemy_unit_counts[VehicleType.FIGHTER] - self.pf.my_unit_counts[VehicleType.FIGHTER]
        for f in [f for f in self.world.facilities if f.owner_player_id == self.me.id]:
            if f.type == FacilityType.CONTROL_CENTER:
                continue
            if f.vehicle_type is None:
                if fighters_diff > -5:
                    self.move_manager.plan_building(f.id, VehicleType.FIGHTER)
                else:
                    self.move_manager.plan_building(f.id, VehicleType.TANK)
            if f.vehicle_type is VehicleType.FIGHTER:
                if fighters_diff < -30:
                    self.move_manager.plan_building(f.id, VehicleType.TANK)
            if f.vehicle_type is VehicleType.TANK:
                if self.pf.my_unit_counts[VehicleType.TANK] > 500:
                    self.move_manager.plan_building(f.id, VehicleType.HELICOPTER)

    def print_heatmap_for(self, field, filename):
        fig, ax = pyplot.subplots(figsize=(25, 25))
        if not hasattr(self, "sns_plot"):
            self.sns_plot = sns.heatmap(pd.DataFrame(field), ax=ax, annot=True, annot_kws={"fontsize": 5}, fmt='g')

        fig = self.sns_plot.get_figure()
        fig.savefig("{}.png".format(filename))
        fig.clf()
