from model.ActionType import ActionType
import random
import math

from operator import itemgetter

import os


class MoveManager:
    def __init__(self, world, game):
        random.seed(game.random_seed)

        self.game = game

        self.moves = []
        self.past_moves = [None] * game.tick_count
        self.world = world

        self.base_action_count = game.base_action_count
        self.max_action_count = self.base_action_count

        self.action_cd = 0

    def update_state(self, world, action_cd, additional_actions=0):
        self.world = world
        self.max_action_count = self.base_action_count + additional_actions
        self.action_cd = action_cd

    @property
    def remaining_actions(self):
        tick = self.world.tick_index
        tick_border = max(tick - 59, 0)
        return self.max_action_count - sum(x is not None for x in self.past_moves[tick_border:tick])

    def make_move(self, move, vehicles):
        if len(self.moves) == 0:
            return False

        self.moves = sorted(self.moves, key=itemgetter('priority'), reverse=True)

        if self.remaining_actions == 0:
            return False

        cur_move = self.moves[0]

        if "wait" in cur_move and cur_move["tick"] + cur_move["wait"] > self.world.tick_index:
            return

        for k, v in cur_move.items():
            if k == "tick":
                continue
            if k == "debug":
                print(self.world.tick_index)
            setattr(move, k, v)

        if cur_move["action"] == ActionType.MOVE:
            for v in vehicles:
                if v.selected:
                    v.move_x = v.x + cur_move["x"]
                    v.move_y = v.y + cur_move["y"]

        self.past_moves[self.world.tick_index] = self.moves.pop(0)
        return True

    def append_move(self, move):
        self.moves.append(move)

    def plan_group_assignment(self, group_id):
        self.append_move({
            **self.default_move,
            "action": ActionType.ASSIGN,
            "group": group_id,
        })

    def plan_nuking(self, x, y, vehicle_id):
        self.append_move({
            **self.default_move,
            "action": ActionType.TACTICAL_NUCLEAR_STRIKE,
            "x": x,
            "y": y,
            "vehicle_id": vehicle_id,
        })

    def plan_rotating(self, x, y, angle=None):
        if angle is None:
            angle = math.pi if random.random() > 0.5 else -math.pi

        self.append_move({
            **self.default_move,
            "action": ActionType.ROTATE,
            "x": x,
            "y": y,
            "angle": angle
        })

    def plan_moving(self, x, y, right=None, bottom=None, vehicle_type=None, max_speed=99.9, group_id=None, select=True):
        if right is None:
            right = self.world.width
        if bottom is None:
            bottom = self.world.height

        if select:
            if group_id is None:
                self.append_move({
                    **self.default_move,
                    "action": ActionType.CLEAR_AND_SELECT,
                    "right": right,
                    "bottom": bottom,
                    "vehicle_type": vehicle_type})
            else:
                self.append_move({
                    **self.default_move,
                    "action": ActionType.CLEAR_AND_SELECT,
                    "group": group_id})

        self.append_move({
            **self.default_move,
            "action": ActionType.MOVE,
            "max_speed": max_speed,
            "x": x,
            "y": y,
        })

    def plan_selecting(self, left_top=None, right_bottom=None, vehicle_type=None, group_id=0, priority=1, add=False):
        left = 0 if left_top is None else left_top[0]
        top = 0 if left_top is None else left_top[1]

        right = self.world.width if right_bottom is None else right_bottom[0]
        bottom = self.world.height if right_bottom is None else right_bottom[1]

        action = ActionType.CLEAR_AND_SELECT
        if add:
            action = ActionType.ADD_TO_SELECTION

        self.append_move({
            **self.default_move,
            "action": action,
            "priority": priority,
            "top": top,
            "left": left,
            "right": right,
            "bottom": bottom,
            "group": group_id,
            "vehicle_type": vehicle_type})

    def plan_scaling(self, x, y, max_speed=99.9, factor=0.1, wait=0, priority=1):
        self.append_move({
            **self.default_move,
            "action": ActionType.SCALE,
            "priority": priority,
            "factor": factor,
            "wait": wait,
            "x": x,
            "y": y,
            "max_speed": max_speed,
        })

    def plan_evading(self, strike_x, strike_y, time_till_strike):
        r = self.game.tactical_nuclear_strike_radius * 1.5
        self.plan_selecting([strike_x - r, strike_y - r], [strike_x + r, strike_y + r], priority=2)
        self.plan_scaling(strike_x, strike_y, factor=10.0, priority=2)
        self.plan_scaling(strike_x, strike_y, factor=0.1, wait=time_till_strike, priority=2)

    def plan_building(self, f_id, vehicle_type, priority=0):
        self.append_move({
            **self.default_move,
            "action": ActionType.SETUP_VEHICLE_PRODUCTION,
            "priority": priority,
            "facility_id": f_id,
            "vehicle_type": vehicle_type,
        })

    @property
    def default_move(self):
        return {
            "tick": self.world.tick_index,
            "priority": 1
        }

    @staticmethod
    def debug():
        return os.getenv('DEBUG_STRATEGY', False)